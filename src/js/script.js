import { openPopup } from './modules/popups.js';
import { getToken } from './modules/login.js';
import { loadVisits } from './modules/visitController.js';
import { noVisitsInfo } from './modules/main.js';
import { loadModal } from './modules/modal.js';

window.addEventListener('load', function () {
  // event for edit/add visit
  const visitEditModal = document.querySelector(
    '[data-popup-link="form__visit-edit"]'
  );
  visitEditModal.addEventListener('click', () => loadModal(null));

  // event for popup-window
  const openPopupBtns = document.querySelectorAll('.popup-link');
  openPopupBtns.forEach((btn) => {
    btn.addEventListener('click', () => openPopup(btn.dataset.popupLink));
  });

  // get token from localStorage
  const token = getToken();
  if (token) {
    // initialize get/draw visits
    loadVisits();
  } else {
    // show div 'No items have been added'
    noVisitsInfo();
  }
});
