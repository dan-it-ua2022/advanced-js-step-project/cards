import { contentVisitsCss, parentVisitCss } from './visitController.js';

export const parentFilter = document.querySelector('.main__search');
const searchTitle = document.querySelector('.search__title');
const searchCompleted = document.querySelector('.search__completed');
const searchUrgency = document.querySelector('.search__urgency');

// event for input data in filter
parentFilter.addEventListener('input', () => filterHandler());

// clear filter when we press Escape btn
parentFilter.addEventListener('keydown', (e) => clearHandler(e));

// filtering visits
function filterHandler() {
  const allVisits = document.querySelectorAll(
    `.${contentVisitsCss} .${parentVisitCss}`
  );
  const valueTitle = searchTitle.value.toLowerCase().trim();
  const valueCompleted = searchCompleted.value;
  const valueUrgetncy = searchUrgency.value;
  const textCompleted =
    searchCompleted.options[searchCompleted.selectedIndex].text;
  const textUrgetncy = searchUrgency.options[searchUrgency.selectedIndex].text;

  // check visit data
  allVisits.forEach((visit) => {
    const visitId = visit.dataset.visitId;
    const visitDiv = document.querySelector(`[data-visit-id="${visitId}"]`);

    const visitFullname = document
      .querySelector(`[data-visit-id="${visitId}"] .visit__fullname`)
      .innerHTML.toLocaleLowerCase()
      .trim();

    const visitBrief = document
      .querySelector(`[data-visit-id="${visitId}"] .brief__value`)
      .innerHTML.toLocaleLowerCase()
      .trim();

    const visitCompleted = document.querySelector(
      `[data-visit-id="${visitId}"] .completed__value`
    ).innerHTML;

    const visitUrgency = document.querySelector(
      `[data-visit-id="${visitId}"] .urgency__value`
    ).innerHTML;

    // clear all visit
    visitDiv.classList.remove('hidden');

    // filtering visit
    if (valueCompleted !== 'null' && visitCompleted !== textCompleted) {
      visitDiv.classList.add('hidden');
    }
    if (valueUrgetncy !== 'null' && visitUrgency !== textUrgetncy) {
      visitDiv.classList.add('hidden');
    }
    if (valueTitle !== '') {
      if (
        visitBrief.search(valueTitle) === -1 &&
        visitFullname.search(valueTitle) === -1
      ) {
        visitDiv.classList.add('hidden');
      }
    }
  });
}

// clear filter when press Esc
function clearHandler(e) {
  if (e.key === 'Escape' || e.keyCode === 27) {
    searchTitle.value = '';
    searchCompleted.value = null;
    searchUrgency.value = null;
    parentFilter.dispatchEvent(new Event('input'));
  }
}
