import { insertNewVisit, insertEditVisit } from './visitController.js';
import { popups } from './popups.js';
import { parentFilter } from './filter.js';

const cssFormContent = 'visit-edit';
const parentModal = document.querySelector('.form__visit-edit');

class Modal {
  constructor(visit = null, element) {
    this.visit = visit;
    this.id = visit ? visit.id : null;
    this.element = element;
    this.popupDataSet = element.dataset.popup;
    this.form = null;
    this.divMoreInfo = null;
    this.divMoreInfoDoctor = null;
    this.btnSubmit = null;
  }

  render() {
    this.element.innerHTML = '';
    this.form = document.createElement('form');
    this.form.classList.add(cssFormContent);

    const modalLayout = `
    <input class="visit-edit__fullname" placeholder="ФИО" required />
    <select class="visit-edit__doctor" required>
      <option value="null">- выбрать врача -</option>
      <option value="cardiologist">Кардиолог</option>
      <option value="dentist">Стоматолог</option>
      <option value="therapist">Терапевт</option>
      Кардиолог
    </select>
    <div class="visit-edit__more-info hidden">
      <select class="visit-edit__completed" required>
        <option value="open">визит запланирован</option>
        <option value="done">визит прошел</option>
      </select>
      <select class="visit-edit__urgency" required>
        <option value="high">неотложная</option>
        <option value="normal">приоритетная</option>
        <option value="low">обычная</option>
      </select>
      <input class="visit-edit__purpose" placeholder="цель визита" required />
      <input
        class="visit-edit__brief"
        placeholder="краткое описание визита"
        required
      />
       <div class="visit-edit__more-info-doctor"></div>
    </div>
    <input
      type="submit"
      class="visit-edit__submit hidden btn shadow"
      value="Отправить"
    />
    `;
    this.form.innerHTML = modalLayout;
    this.element.append(this.form);

    // get more-info div
    this.divMoreInfo = document.querySelector(
      `[data-popup="${this.popupDataSet}"] .visit-edit__more-info`
    );

    // get more-info-doctor div for different doctors
    this.divMoreInfoDoctor = document.querySelector(
      `[data-popup="${this.popupDataSet}"] .visit-edit__more-info-doctor`
    );

    // get/event btn-submit form
    this.btnSubmit = document.querySelector(
      `[data-popup="${this.popupDataSet}"] .visit-edit__submit`
    );
    this.btnSubmit.addEventListener('click', (e) => {
      e.preventDefault();
      addVisit(this.popupDataSet, this.id);
    });

    // get additional inputs for different doctors
    const doctor = document.querySelector(
      `[data-popup="${this.popupDataSet}"] .visit-edit__doctor`
    );
    doctor.addEventListener('change', () => {
      switch (doctor.value) {
        case 'cardiologist':
          this.moreCardiologist();
          break;
        case 'dentist':
          this.moreDentist();
          break;
        case 'therapist':
          this.moreTherapist();
          break;
        default:
          return;
      }
    });

    // show additional inputs
    if (this.visit) {
      doctor.value = this.visit.doctor;
      doctor.dispatchEvent(new Event('change'));
      setDataVisit(this.popupDataSet, this.visit);
    }
  }

  // additional data for Cardiologist
  moreCardiologist() {
    this.divMoreInfoDoctor.innerHTML = '';
    const moreLayout = `
      <input
        class="visit-edit__pressure"
        placeholder="обычное давление"
        required
      />
      <input
        class="visit-edit__mass-index"
        placeholder="индекс массы тела"
        required
      />
      <input
        class="visit-edit__disease"
        placeholder="перенесенные заболевания"
        required
      />
      <input class="visit-edit__age" placeholder="возраст" required />      
    `;
    this.divMoreInfoDoctor.insertAdjacentHTML('beforeend', moreLayout);
    this.divMoreInfo.classList.remove('hidden');
    this.btnSubmit.classList.remove('hidden');
  }

  // additional data for Dentist
  moreDentist() {
    this.divMoreInfoDoctor.innerHTML = '';
    const moreLayout = `
      <input
        class="visit-edit__last-date-visit"
        placeholder="дата последнего посещения"
        required
      />      
    `;
    this.divMoreInfoDoctor.insertAdjacentHTML('beforeend', moreLayout);
    this.divMoreInfo.classList.remove('hidden');
    this.btnSubmit.classList.remove('hidden');
  }

  // additional data for Therapist
  moreTherapist() {
    this.divMoreInfoDoctor.innerHTML = '';
    const moreLayout = `
      <input class="visit-edit__age" placeholder="возраст" required />      
    `;
    this.divMoreInfoDoctor.insertAdjacentHTML('beforeend', moreLayout);
    this.divMoreInfo.classList.remove('hidden');
    this.btnSubmit.classList.remove('hidden');
  }
}

function setDataVisit(popupDataSet, visit) {
  const fullname = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__fullname`
  );
  fullname.value = visit.fullname;

  const doctor = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__doctor`
  );
  doctor.value = visit.doctor;

  const completed = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__completed`
  );
  completed.value = visit.completed;

  const urgency = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__urgency`
  );
  urgency.value = visit.urgency;

  const purpose = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__purpose`
  );
  purpose.value = visit.purpose;

  const brief = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__brief`
  );
  brief.value = visit.brief;

  switch (doctor.value) {
    case 'cardiologist':
      const pressure = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__pressure`
      );
      pressure.value = visit.pressure;

      const massIndex = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__mass-index`
      );
      massIndex.value = visit.massIndex;

      const disease = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__disease`
      );
      disease.value = visit.disease;

      const age = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__age`
      );
      age.value = visit.age;
      break;

    case 'dentist':
      const lastDateVisit = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__last-date-visit`
      );
      lastDateVisit.value = visit.lastDateVisit;
      break;

    case 'therapist':
      const age2 = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__age`
      );
      age2.value = visit.age;
      break;

    default:
      return;
  }
}

function addVisit(popupDataSet, visitId = null) {
  const newVisit = {};

  newVisit.id = visitId;

  const fullname = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__fullname`
  );
  newVisit.fullname = fullname.value;

  const doctor = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__doctor`
  );
  newVisit.doctor = doctor.value;

  const completed = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__completed`
  );
  newVisit.completed = completed.value;

  const urgency = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__urgency`
  );
  newVisit.urgency = urgency.value;

  const purpose = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__purpose`
  );
  newVisit.purpose = purpose.value;

  const brief = document.querySelector(
    `[data-popup="${popupDataSet}"] .visit-edit__brief`
  );
  newVisit.brief = brief.value;

  switch (doctor.value) {
    case 'cardiologist':
      const pressure = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__pressure`
      );
      newVisit.pressure = pressure.value;

      const massIndex = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__mass-index`
      );
      newVisit.massIndex = massIndex.value;

      const disease = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__disease`
      );
      newVisit.disease = disease.value;

      const age = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__age`
      );
      newVisit.age = age.value;
      break;

    case 'dentist':
      const lastDateVisit = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__last-date-visit`
      );
      newVisit.lastDateVisit = lastDateVisit.value;
      break;

    case 'therapist':
      const age2 = document.querySelector(
        `[data-popup="${popupDataSet}"] .visit-edit__age`
      );
      newVisit.age = age2.value;
      break;

    default:
      return;
  }

  // validation data
  validation(newVisit);
}

// validation data
async function validation(visit) {
  // check empty value
  const empty = Object.values(visit).find((item) => item === '');
  if (empty !== undefined) {
    alert('Все поля должны быть заполнены !!!');
    return;
  }

  // check doctor filled
  if (
    visit.doctor !== 'cardiologist' &&
    visit.doctor !== 'dentist' &&
    visit.doctor !== 'therapist'
  ) {
    alert('Выберите доктора');
    return;
  }

  // if validation completed we insert new/edit visit on window
  if (visit.id) {
    // insert edit visit
    await insertEditVisit(visit);
  } else {
    // insert new visit
    await insertNewVisit(visit);
  }

  // close popup
  popups.click();

  // filtering visit if filter be filling
  parentFilter.dispatchEvent(new Event('input'));
}

export function loadModal(visit) {
  const modalView = new Modal(visit, parentModal);
  modalView.render();
}
