import { popups } from './popups.js';
import { loadVisits } from './visitController.js';
import { noVisitsInfo } from './main.js';

const createBtn = document.querySelector('.create-btn');
const loginBtn = document.querySelector('.login-btn');
const logoutBtn = document.querySelector('.logout-btn');

const URL_LOGIN = 'https://ajax.test-danit.com/api/v2/cards/login';
const loginEmail = document.querySelector('.login__email');
const loginPassword = document.querySelector('.login__password');
const loginSubmit = document.querySelector('.login__submit');

/**
 * This code for development
 */
{
  const emailTMP = 'portnov.dmitry1@gmail.com';
  const passwordTMP = '12345';
  loginEmail.value = emailTMP;
  loginPassword.value = passwordTMP;
}
/********************************************/

// login event
loginSubmit.addEventListener('click', (e) => login(e));

// logout event
logoutBtn.addEventListener('click', () => {
  changeLogoutBtn();
  removeToken();
  noVisitsInfo();
});

// login
async function login(e) {
  try {
    e.preventDefault();
    const response = await fetch(URL_LOGIN, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: loginEmail.value,
        password: loginPassword.value,
      }),
    });

    const token = await response.text();
    if (!token) {
      throw new Error('Token is empty');
    }

    setToken(token);
    loadVisits();
  } catch (error) {
    console.log(error);
  } finally {
    popups.click();
  }
}

// if the login was successful, set token in localStorage
function setToken(token) {
  localStorage.setItem('token', token);
}

// get token from localStorage
export function getToken() {
  return localStorage.getItem('token') ? localStorage.getItem('token') : null;
}

// remove token if user was logout
function removeToken() {
  localStorage.removeItem('token');
}

// hide/show login/logout/createVisit-btn for login/logout user
export function changeLoginBtn() {
  createBtn.classList.add('preshow');
  logoutBtn.classList.add('preshow');
  loginBtn.classList.add('prehidden');

  setTimeout(() => {
    createBtn.classList.add('show');
    logoutBtn.classList.add('show');
    loginBtn.classList.add('hidden');
  }, 0);
}

// hide/show login/logout/createVisit-btn for login/logout user
export function changeLogoutBtn() {
  createBtn.classList.remove('show');
  logoutBtn.classList.remove('show');
  loginBtn.classList.remove('prehidden');

  setTimeout(() => {
    createBtn.classList.remove('preshow');
    logoutBtn.classList.remove('preshow');
    loginBtn.classList.remove('hidden');
  }, 300);
}
