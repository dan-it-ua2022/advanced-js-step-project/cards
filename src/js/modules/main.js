import { contentVisits } from './visitController.js';

// info for div.content__cards when we don't have any cards or don't login
export function noVisitsInfo() {
  contentVisits.innerHTML =
    '<div class="no-visits"><span>No items have been added</span></div>';
}
