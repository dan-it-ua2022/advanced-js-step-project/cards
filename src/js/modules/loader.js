// show loader popup when we loading visits from server
export function showLoader() {
  const loader = document.querySelector(`.loader`);
  loader.classList.add('preshow');

  setTimeout(() => {
    loader.classList.add('show');
  }, 0);
}

// hide loader popup when we completed loading visits from server
export function hideLoader() {
  const loader = document.querySelector(`.loader`);
  loader.classList.remove('show');

  setTimeout(() => {
    loader.classList.remove('preshow');
  }, 300);
}
