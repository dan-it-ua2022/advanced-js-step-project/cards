// main class Visit
export class Visit {
  constructor(
    visitId = null,
    fullname,
    doctor,
    purpose,
    brief,
    urgency,
    completed
  ) {
    this.id = visitId;
    this.fullname = fullname;
    this.doctor = doctor;
    this.purpose = purpose;
    this.brief = brief;
    this.urgency = urgency;
    this.completed = completed;
  }
}
