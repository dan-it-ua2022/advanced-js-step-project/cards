import { getToken } from './login.js';

const URL_VISITS = `https://ajax.test-danit.com/api/v2/cards`;

// obj for CUDL with server DB
export const dataService = {
  get allVisits() {
    return fetch(URL_VISITS, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .catch((error) => console.log(error));
  },

  getVisit(visitId) {
    return fetch(`${URL_VISITS}/${visitId}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .catch((error) => console.log(error));
  },

  createVisit(visit) {
    return fetch(URL_VISITS, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(visit),
    }).catch((error) => console.log(error));
  },

  updateVisit(visitId, visit) {
    return fetch(`${URL_VISITS}/${visitId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(visit),
    }).catch((error) => console.log(error));
  },

  deleteVisit(visitId) {
    return fetch(`${URL_VISITS}/${visitId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    }).catch((error) => console.log(error));
  },
};

/*************** this code for development *******************/

// const cardTest1 = {
//   fullname: 'Семен Семенович Горбунков',
//   doctor: 'cardiologist',
//   purpose: 'Консультация',
//   brief: 'Плановый визит',
//   urgency: 'high',
//   completed: 'open',
//   pressure: '90/120',
//   massIndex: 23,
//   disease: 'ишемия',
//   age: 55,
// };

// const cardTest2 = {
//   fullname: 'Григорьев Валентин Васильевич',
//   doctor: 'dentist',
//   purpose: 'Чистка клыков',
//   brief: 'Ежегодная плановая операция',
//   urgency: 'normal',
//   completed: 'done',
//   lastDateVisit: '22-10-2023',
// };

// const cardTest3 = {
//   fullname: 'Рустам Генадиевич Растопыркин',
//   doctor: 'therapist',
//   purpose: 'Проверка',
//   brief: 'Выписка',
//   urgency: 'low',
//   completed: 'open',
//   age: 33,
// };

// const cardTest4 = {
//   fullname: 'Виталий Григорьевич Патолак',
//   doctor: 'cardiologist',
//   purpose: 'Проверка отклика',
//   brief: 'Проверить отклик от 3 нерва',
//   urgency: 'high',
//   completed: 'open',
//   pressure: '85/115',
//   massIndex: 21,
//   disease: 'Покраснение левой нижней конечности',
//   age: 78,
// };

// const cardTest5 = {
//   fullname: 'Николай Аргентович Симонов',
//   doctor: 'dentist',
//   purpose: 'Кариес',
//   brief: 'Ультрозвуковая чистка',
//   urgency: 'normal',
//   completed: 'done',
//   lastDateVisit: '14-05-2020',
// };

// const cardTest6 = {
//   fullname: 'Леонид Павлович Чавкар',
//   doctor: 'therapist',
//   purpose: 'Выписка справки',
//   brief: 'Осмотр по страховке',
//   urgency: 'low',
//   completed: 'open',
//   age: 25,
// };

//****** create visit from obj **************************************
// dataService.createVisit(cardTest1).then((response) => console.log(response.ok)); // add one card test
// dataService.createVisit(cardTest2).then((response) => console.log(response.ok)); // add one card test
// dataService.createVisit(cardTest3).then((response) => console.log(response.ok)); // add one card test
// dataService.createVisit(cardTest4).then((response) => console.log(response.ok)); // add one card test
// dataService.createVisit(cardTest5).then((response) => console.log(response.ok)); // add one card test
// dataService.createVisit(cardTest6).then((response) => console.log(response.ok)); // add one card test
//****************************************************************/

//****** delete card, need id *****************************************
// dataService.deleteVisit(144460).then((response) => console.log(response.ok));
//****************************************************************/

//****** update card, need id *****************************************
// dataService
//   .updateVisit(144459, { title: 'test44' })
//   // .then((response) => console.log(response.ok))
//   .then((response) => response.json())
//   .then((data) => console.log(data));
//****************************************************************/

//****** load all visit ******************************************
// const result = dataService.allVisits
//   .then((data) => console.log(data))
//   .catch((error) => console.log(error));
//****************************************************************/
