import { Visit } from './visit.js';
import {
  visitHandler,
  getDoctor,
  getUrgency,
  getCompleted,
  parentVisitCss,
  parentVisitShadowCss,
} from './visitController.js';

export class VisitDentist extends Visit {
  constructor(
    visitId,
    fullname,
    doctor,
    purpose,
    brief,
    urgency,
    completed,
    lastDateVisit
  ) {
    super(visitId, fullname, doctor, purpose, brief, urgency, completed);
    this.lastDateVisit = lastDateVisit;
  }
}

// create VisitDentist layout
export class VisitDentistTemplate {
  constructor(visit) {
    this.visit = visit;
    this.div = null;
  }

  render(element) {
    this.div = document.createElement('div');
    this.div.classList.add(parentVisitCss);
    this.div.classList.add(parentVisitShadowCss);
    this.div.dataset.visitId = this.visit.id;

    let doctor = getDoctor(this.visit.doctor);
    let urgency = getUrgency(this.visit.urgency);
    let completed = getCompleted(this.visit.completed);

    let visitLayout = `
      <div class="visit__btn-control">
        <button class="btn-more">
          <i class="fa-solid fa-file-circle-info"></i>
        </button>
        <button class="btn-edit" data-popup-id="form__visit-edit">
          <i class="fa-solid fa-file-pen"></i>
        </button>
        <button class="btn-delete">
          <i class="fa-solid fa-trash"></i>
        </button>
      </div>
      <div class="visit__fullname">${this.visit.fullname}</div>
      <div class="visit__doctor">${doctor}</div>
      <div class="visit__more-info">
       <div class="visit__completed">
          статус:
          <span class="completed__value">${completed}</span>
        </div>
         <div class="visit__urgency">
          срочность:
          <span class="urgency__value">${urgency}</span>
        </div>
        <div class="visit__purpose">
          цель визита:
          <span class="purpose__value">${this.visit.purpose}</span>
        </div>
        <div class="visit__brief">
          краткое описание визита:
          <span class="brief__value">${this.visit.brief}</span>
        </div>   
        <div class="visit__last-date-visit">
          дата последнего посещения:
          <span class="last-date-visit__value">${this.visit.lastDateVisit}</span>
        </div>
      </div>   
    `;

    this.div.insertAdjacentHTML('beforeend', visitLayout);
    element.append(this.div);

    // event for show more info/edit/delete
    this.div.addEventListener('click', (e) => visitHandler(e));
  }
}
