const CLASS_LINK_POPUP = 'data-popup-link';
const CLASS_POPUPS = '.popups';
const CLASS_POPUPS_OVERLAY = '.popups-overlay';
const CLASS_POPUPS_BODY = '.popups-body';
const CLASS_CLOSE_POPUP = '.close-popup';
let dataLink = '';

// event for close popup when we click op close-popup-btn
const closePopupBtn = document.querySelector(CLASS_CLOSE_POPUP);
if (closePopupBtn) {
  closePopupBtn.addEventListener('click', (e) => {
    e.preventDefault();
    closePopups(closePopupBtn.closest(CLASS_POPUPS));
  });
}

// event for close popup when we click on popup-overlay
export const popups = document.querySelector(CLASS_POPUPS);
if (popups) {
  popups.addEventListener('click', (e) => {
    if (!e.target.closest(CLASS_POPUPS_BODY)) {
      closePopups(e.target.closest(CLASS_POPUPS));
    }
  });
}

// open popup window
export function openPopup(dataPopup) {
  dataLink = dataPopup;
  const target = document.querySelector(`[data-popup=${dataPopup}]`);

  popups.classList.add('preshow');
  target.classList.add('preshow');

  setTimeout(() => {
    popups.classList.add('show');
    target.classList.add('show');
  }, 0);
}

// close popup window
function closePopups(target) {
  const popup = document.querySelector(`[data-popup=${dataLink}]`);

  popup.classList.remove('show');
  target.classList.remove('show');

  setTimeout(() => {
    popup.classList.remove('preshow');
    target.classList.remove('preshow');
  }, 300);
}
